use crate::Image;
use image::imageops;

type Bounds = (x, y, width, height);

pub fn trim(img: &mut Image) {
    let (x, y, width, height) = trim_bounds(img);
    imageops::crop(img, x, y, width, height);
}

fn trim_bounds(img: &Image) -> Bounds {

}
